/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./hw/composition.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./hw/composition.js":
/*!***************************!*\
  !*** ./hw/composition.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const MakeBackendMagic = state => ({\n  makeBackendMagic: () => console.log('Backend magic!')\n});\n\nconst MakeFrontendMagic = state => ({\n  makeFrontendMagic: () => console.log('Frontend magic!')\n});\n\nconst MakeItLooksBeautiful = state => ({\n  makeItLooksBeautiful: () => console.log('Looks beautiful!')\n});\n\nconst DistributeTasks = state => ({\n  distributeTasks: () => console.log('Distribute!')\n});\n\nconst DrinkSomeTea = state => ({\n  drinkSomeTea: () => console.log('Drink tea')\n});\n\nconst WatchYoutube = state => ({\n  watchYoutube: () => console.log('Whatch youtube')\n});\n\nconst Procrastinate = state => ({\n  procrastinate: () => console.log('Procrastinate')\n});\n\nfunction SomeRobot(name) {\n  this.name = name;\n  this.speed = speed;\n  return Object.assign(this, Move(this), LoggerIn(this));\n}\n\nfunction BackendDeveloper(name, gender, age) {\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = 'back';\n  return Object.assign(this, MakeFrontendMagic(this), DrinkSomeTea(this), Procrastinate(this));\n}\n\nfunction FrontendDeveloper(name, gender, age) {\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = 'front';\n  return Object.assign(this, MakeBackendMagic(this), DrinkSomeTea(this), WatchYoutube(this));\n}\n\nfunction Designer(name, gender, age) {\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = 'designer';\n  return Object.assign(this, MakeItLooksBeautiful(this), WatchYoutube(this), Procrastinate(this));\n}\n\nfunction ProjectManager(name, gender, age) {\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = 'project';\n  return Object.assign(this, DistributeTasks(this), Procrastinate(this), DrinkSomeTea(this));\n}\n\nlet pr = new ProjectManager('name', 'male', 12);\nconsole.log(pr);\n\n//# sourceURL=webpack:///./hw/composition.js?");

/***/ })

/******/ });