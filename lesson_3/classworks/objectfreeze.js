/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/
let universe = {
  infinity: Infinity,
  good: ['cats', 'love', 'rock-n-roll'],
  evil: {
    bonuses: ['cookies', 'great look']
  },

  test: {
    test: {
      test: {
        test: {
          test: [1, 2, 3, 4]
        }
      }
    }
  }
};

let DeepFreeze = (obj) => {
  let frozen = Object.freeze(obj);
  let props = Object.getOwnPropertyNames(obj);
  props.forEach( (prop) => {
    if (typeof (obj[prop]) == 'object' && obj[prop] !== null) {
      return DeepFreeze(obj[prop]);
    }
  });

  return frozen;
};

const work = () => {
  console.log( 'your code ');
  let FarGalaxy = DeepFreeze(universe);
      //FarGalaxy.good.push('javascript'); // false
      //FarGalaxy.something = 'Wow!'; // false
      //FarGalaxy.evil.humans = [];   // false
      FarGalaxy.test.test.test.test.lol = 1;
      console.log(FarGalaxy);
}

export default work;
