import Goverment from './singleton'

const work = () => {
    console.log(Goverment.readConstitution());
    Goverment.addLaw({ id: 1, name: '12312', description: 'test' });
    Goverment.addLaw({ id: 2, name: '12312', description: 'test1' });
    Goverment.addLaw({ id: 3, name: '12312', description: 'test2' });
    Goverment.addLaw({ id: 4, name: '12312', description: 'test3' });
    console.log(Goverment.readConstitution());
    console.log(Goverment.readLaw(2));
    console.log(Goverment.showBudget());
    console.log(Goverment.showCitizensSatisfactions());
    Goverment.newHoliday();
    console.log(Goverment.showBudget());
    console.log(Goverment.showCitizensSatisfactions());
};

export default work;