/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

const _data = {
  laws: [
    {
      id: 0,
      name: '123123',
      description: '1231434'
    }
  ],
  budget: 1000000,
  citizensSatisfactions: 0
};

const Store = {
  addLaw: ( {id, name, description} ) => _data.laws.push({id, name, description}),
  readConstitution: () => [ ..._data.laws ],
  readLaw: (id) => _data.laws.find( d => d.id === id ),
  showCitizensSatisfactions: () => _data.citizensSatisfactions,
  showBudget: () => _data.budget,
  newHoliday: () => {
    _data.budget -= 50000;
    _data.citizensSatisfactions += 5;
  }
};

Object.freeze(Store);

export default Store;
