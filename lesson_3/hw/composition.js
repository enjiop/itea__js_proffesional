const MakeBackendMagic = state => ({
    makeBackendMagic: () => console.log('Backend magic!')
});

const MakeFrontendMagic = state => ({
    makeFrontendMagic: () => console.log('Frontend magic!')
});

const MakeItLooksBeautiful = state => ({
    makeItLooksBeautiful: () => console.log('Looks beautiful!')
});

const DistributeTasks = state => ({
    distributeTasks: () => console.log('Distribute!')
});

const DrinkSomeTea = state => ({
    drinkSomeTea: () => console.log('Drink tea')
});

const WatchYoutube = state => ({
    watchYoutube: () => console.log('Whatch youtube')
});

const Procrastinate = state => ({
    procrastinate: () => console.log('Procrastinate')
});

export function BackendDeveloper( name, gender, age, type = 'back' ) {
    this.name = name;
    this.gender = gender;
    this.age = age;

    this.type = type;

    return Object.assign(
        this,
        MakeFrontendMagic(this),
        DrinkSomeTea(this),
        Procrastinate(this)
    );
}

export function FrontendDeveloper( name, gender, age, type = 'front' ) {
    this.name = name;
    this.gender = gender;
    this.age = age;

    this.type = type;

    return Object.assign(
        this,
        MakeBackendMagic(this),
        DrinkSomeTea(this),
        WatchYoutube(this)
    );
}

export function Designer( name, gender, age, type = 'designer' ) {
    this.name = name;
    this.gender = gender;
    this.age = age;

    this.type = type;

    return Object.assign(
        this,
        MakeItLooksBeautiful(this),
        WatchYoutube(this),
        Procrastinate(this)
    );
}

export function ProjectManager( name, gender, age, type = 'project' ) {
    this.name = name;
    this.gender = gender;
    this.age = age;

    this.type = type;

    return Object.assign(
        this,
        DistributeTasks(this),
        Procrastinate(this),
        DrinkSomeTea(this)
    );
}