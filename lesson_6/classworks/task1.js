/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/

class Human {
  constructor(name){
    this.name = name;
    this.currentTemperature = 0;
    this.minTemperature = -10;
    this.maxTemperature = 50;

    console.log(`new Human ${this.name} arrived!`, this);
  }

  changeTemperature(changeValue){
    console.log(
      'current', this.currentTemperature + changeValue,
      'min', this.minTemperature,
      'max', this.maxTemperature
    );

    this.currentTemperature += changeValue;
  }
}

class CoolerHuman extends Human {
  constructor(name) {
    super(name);
    this.coolers = [
      {name: 'icecream', temperatureCoolRate: -5},
      {name: 'water', temperatureCoolRate: -3},
      {name: 'juice', temperatureCoolRate: -2}
    ];

    this.highTemperature = 30;

    console.log(`Cooler Human ${name}`, this);
  }

  changeTemperature(changeValue) {
    let prevTemperature = this.currentTemperature;
    super.changeTemperature();

    if ( this.currentTemperature > this.highTemperature ) {
      this.maxTemperature = this.maxTemperature - this.coolers.reduce(
          (currentCoolRate, cooler) => {
            return currentCoolRate + cooler.temperatureCoolRate;
          }, 0
        );
    }

    if ( this.currentTemperature > this.maxTemperature ) {
      console.error(`Temperature is too high: ${this.currentTemperature}. ${this.name} is died :(`);
    } else {
      if ( this.currentTemperature < prevTemperature ) {
        console.log(`Temperature is falling.`);
      } else {
        console.log(`It's hot outside (${this.currentTemperature} deg), please cool yourself, or or ${this.name} will die!`);
      }
    }
  }
}

const BeachParty = () => {

  console.log( 'your code ');

  let Name = new CoolerHuman('Name');
    Name.changeTemperature(25);
    Name.changeTemperature(40);

}

export default BeachParty;
