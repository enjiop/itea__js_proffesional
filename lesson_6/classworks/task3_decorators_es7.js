/*

  Задание:
    1. Используя функциональный декоратор, написать декоратор который будет показывать
       аргументы и результат выполнения функции.

    2. Написать декоратор для класса, который будет преобразовывать аргументы в число,
       если они переданы строкой, и выводить ошибку если переданая переменная не
       может быть преобразована в число
*/

const Work1 = () => {

  function toNumberDecorator( obj ) {
    console.log('decorator object', obj);

    if (obj.kind === 'method') {
      let originalFunction = obj.descriptor.value;

      obj.descriptor.value = function( ...args ) {
        let convertedArgs = args.map( arg => Number(arg) );
        let result = originalFunction.apply( this, convertedArgs );
        return result;
      }
    } else {
      console.error('This deecorator can\'t be assigned to field property');
    }
  }
  
  class CoolMath {
    @toNumberDecorator
    addNumbers(a,b){ return a+b; }

    @toNumberDecorator
    multiplyNumbers(a,b){ return a*b}

    @toNumberDecorator
    minusNumbers(a,b){ return a-b }
  }
  let Calcul = new CoolMath();
  let x = Calcul.addNumbers(2, 2)
  let y = Calcul.multiplyNumbers("10", "2")
  let z = Calcul.minusNumbers(10, 2)

  console.log(x, y, z);

};

export default Work1;
