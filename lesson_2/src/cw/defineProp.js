
/*

    Задание, написать класс, который создает объекты по модельке:
    Dog {
        name: "", -> not configurable
        breed: "", -> not configurable, not editable
        weight: "",
        isGoodBoy: true -> enumerable 
    }   

*/

class Dog {
    constructor() {
        Object.defineProperty( this, 'name', {
            value: "name",
            configurable: false
        });

        Object.defineProperty( this, 'breed', {
            value: "breed",
            configurable: false,
            writable: false
        });

        this.weight = "";

        Object.defineProperty( this, 'isGoodBoy', {
            value: true,
            enumerable: true
        });
    }
}

let dog = new Dog();

console.log( dog );