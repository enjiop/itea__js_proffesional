/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          + Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным

    <div class="post">
      <div class="post__title">Some post</div>
      <div class="post__image">
        <img src="https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like!</button>
      </div>
    </div>

*/


class Post {
  constructor({ title, image, description, likes }) {
    this.title = title;
    this.image = image;
    this.description = description;
    this.likes = likes;
  }

  render = (root = document.getElementById('posts_feed') ) => {
    const node = document.createElement('div');

    node.className = 'post';

    node.innerHTML = `
      <div class="post__title">${this.title}</div>
      <div class="post__image">
        <img src="${this.image}"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like! ${this.likes}</button>
      </div>
    `;

    root.appendChild( node );

    const button = node.querySelector('.post__like');

    button.addEventListener('click', this.likePost);
  }

  likePost = ( e ) => {
    this.likes++;
    e.target.textContent = `Likes ${this.likes}`;
  }
}

class Advertisment extends Post{
  constructor( { title, image, description, likes } ) {
    super({ title, image, description, likes });
  }

  buyItem = () => {
    console.log("Buy");
  }

  render = (root = document.getElementById('posts_feed')) => {
    
    const node = document.createElement('div');

    node.className = 'post';

    node.innerHTML = `
      <h1>ADVERTISMENT</h1>
      <div class="post__image">
      
      </div>
      <div class="post__title">${this.title}</div>
      <div class="post__footer">
      <button class="post__like">Like! ${this.likes}</button>
      </div>
      <div class="post__description"></div>
    `;

    root.appendChild( node );

    const button = node.querySelector('.post__like');

    button.addEventListener('click', this.likePost);
  }
}

const postData = {
  title: 'Some Post',
  image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
  description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi, aspernatur deserunt. Accusantium ab error cupiditate eaque? Dignissimos, velit aut sint doloremque consectetur voluptatum minima ducimus laborum tempore labore vitae veritatis, harum saepe ipsam alias nobis laudantium incidunt quaerat explicabo fuga. Ut iusto culpa dolorum voluptate numquam aspernatur, ipsam consequatur rem eum impedit dolor, quia accusamus laborum quo ullam voluptatem amet provident, eligendi id. Blanditiis laudantium reprehenderit corrupti adipisci obcaecati, incidunt porro culpa natus vitae consequatur ad impedit sunt id, cum beatae maxime quas harum magnam explicabo optio rem! Consectetur nulla nihil, consequatur fuga nisi accusamus! Facilis obcaecati excepturi fugiat omnis, magni ad vel voluptas nemo quisquam dolores, quam a corrupti quod natus expedita! Porro velit eos recusandae consectetur? Qui harum sed quo reiciendis temporibus. Doloremque neque nesciunt mollitia ipsum minus! Error vitae exercitationem magni illum nam laborum provident et culpa? Dignissimos omnis praesentium aliquam soluta ratione, ut explicabo provident quia nam? Quidem placeat odit necessitatibus maiores, officia, blanditiis quis sapiente ipsum ipsam facere esse vel. Aliquam consectetur odio in quasi omnis impedit similique itaque corrupti sunt incidunt repellat error non ullam aliquid, modi quos voluptatibus perspiciatis quod, repudiandae dolorum fugit autem nihil repellendus? Soluta ea mollitia dolorem, architecto quia natus harum, minus neque numquam suscipit est similique vel, facere minima aliquid dolor cum quasi? Debitis quis ipsa mollitia praesentium facere placeat quod nisi maiores dolores aperiam animi eveniet, provident ipsum aliquid ex aspernatur ut ab soluta error vero. Maiores fuga, amet cumque modi ea magnam voluptatibus, sapiente ratione illo iusto veniam deleniti mollitia alias quam velit ex voluptatem cum voluptates laudantium tempora, reiciendis aspernatur perferendis. Corporis laborum fugiat illo assumenda sapiente laboriosam atque ad facilis quasi quae suscipit, officiis quod doloremque aperiam dignissimos id velit aliquid aliquam. A, accusantium suscipit non consequuntur ipsam temporibus totam dolor nam. Blanditiis molestias magni assumenda! Harum ab veniam repellendus velit nobis eveniet assumenda corporis id quas sapiente, saepe officia qui. Aliquam dolor soluta vitae maxime ipsa quibusdam maiores voluptatum laboriosam quisquam, facilis optio nemo tempore fugit pariatur quae reiciendis placeat aliquid voluptas! Sequi, dolorum. Omnis expedita laboriosam, doloribus corrupti illum aliquam blanditiis nihil nisi fuga quidem, dolorem incidunt velit quae. Labore velit doloremque dolores at impedit minus, sed fugiat nisi, consequuntur quaerat, ducimus reiciendis porro a quo? Maiores eaque unde culpa mollitia reprehenderit omnis debitis labore ex suscipit, enim eius repellat? Dicta consequuntur molestias nulla amet similique placeat laboriosam perspiciatis facilis nihil voluptatem at rerum dolores nemo, enim, quis sequi perferendis? Quo facilis, distinctio est a reprehenderit fugit? Hic, eveniet incidunt. Ullam nam quibusdam blanditiis tenetur laboriosam eum animi culpa cumque itaque. Quaerat expedita asperiores quas explicabo fugiat molestiae ipsam quia nostrum perferendis suscipit illum at ratione quasi animi repellat sapiente ducimus, sit modi error. Rem obcaecati expedita consectetur, reprehenderit hic fugiat molestias provident libero minus doloremque cumque unde iusto pariatur voluptas omnis aliquam laboriosam alias quam ullam eos, voluptatem qui. Facere, perferendis non praesentium assumenda distinctio recusandae? Dolores eligendi rem sit accusantium blanditiis, porro voluptatem nisi dicta fugit debitis alias aliquid. Omnis, eius.',
  likes: 0
};

document.addEventListener('DOMContentLoaded', () => {
  const posts = [new Post( postData ), new Post( postData ), new Post( postData ), new Post( postData ), new Post( postData ), new Post( postData )];

  posts.forEach((post, index) => {
    if( index !== 0 && index % 2 !== 0 ) {
      const adv = new Advertisment( postData );
      post.render();
      adv.render();
    } else {
      post.render();
    }
  });
});
