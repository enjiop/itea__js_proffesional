import {
    OurMenu,
    Burger
} from './burger';
import {
    OurOrders,
    Order
} from './order';

const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
];

let ham = new Burger('Hamburger', ['Булка', 'Огурчик', 'Котлетка'], 20);
let chis = new Burger('Cheeseburger', ['Булка', 'Огурчик', 'Котлетка', 'Сыр Чеддер'], 25);
let bur = new Burger('SomeBurg', ['Булка', 'Огурчик', 'Котлетка', 'Сыр Виолла', 'Майонез'], 30);

new Order('Hamburger');
new Order('', 'has', 'Майонез');
new Order('', 'has', 'Кунжут');

new Order('', 'except', 'Сыр Чеддер');