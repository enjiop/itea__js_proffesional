import {
    OurMenu
} from "./burger";
import {
    randomInt
} from './helpers';

export let OurOrders = [];

export function Order(name, condition, value) {
    this.id = new Date().getTime();
    this.name = name;
    this.condition = condition;
    this.value = value;

    this.newOrder = function (burger) {
        OurOrders.push(this);
        console.log(`Заказ ${OurOrders.length}: Бургер ${burger.name}, будет готов через ${burger.cookingTime}`);
    }

    this.burgerNotFound = function () {
        let randomBurger = OurMenu[randomInt(0, OurMenu.length - 1)];
        let isAgree = false;

        if (confirm(`К сожалению, такого бургера у нас нет, можете попробовать ${randomBurger.name}`)) {
            this.newOrder(randomBurger);
        } else {
            while (!isAgree) {
                if (confirm(`Можете попробовать ${randomBurger.name}`)) {
                    isAgree = true;
                    this.newOrder(randomBurger);
                } else {
                    isAgree = false;
                    randomBurger = OurMenu[randomInt(0, OurMenu.length - 1)];
                }
            }
        }
    }

    this.order = function () {
        if (this.name !== '') {
            let result = OurMenu.find(item => item.name === this.name);
            this.newOrder(result);
        } else {
            if (this.condition === 'has') {
                let result = OurMenu.find(item => item.ingredients.indexOf(this.value) + 1);
                if (result) {
                    this.newOrder(result);
                } else {
                    this.burgerNotFound();
                }
            } else if (this.condition === 'except') {
                let result = OurMenu.find(item => !(item.ingredients.indexOf(this.value) + 1));
                if (result) {
                    this.newOrder(result);
                } else {
                    this.burgerNotFound();
                }
            }
        }
    };

    this.order();
}