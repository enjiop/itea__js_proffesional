export let OurMenu = [];

export function Burger(name, ingredients, cookingTime) {
    this.name = name;
    this.ingredients = ingredients;
    this.cookingTime = cookingTime;

    OurMenu.push(this);
    console.log('menu:', OurMenu);

    this.showComposition = function() {
        let {ingredients, name} = this;
        let ingredientsLength = ingredients.length;
        console.log(ingredientsLength);

        if( ingredientsLength !== 0 ) {
            ingredients.map( item => console.log('Состав бургера', name, item));
        }
    }
}