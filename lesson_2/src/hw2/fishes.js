import Animal from './animal';

class Fishes extends Animal {
    constructor(name, phrase, foodType, speed) {
        super(name, phrase, foodType);
        this.type = 'shark';
        this.zone = 'fishes';
        this.speed = speed;
    }

    swim() {
        console.log(`${this.type} ${this.name} swim with speed ${this.speed} km/h`);
    }
}

export default Fishes;