class Zoo {
  constructor( name ) {
    this.name = name;
    this.animalCount = 0;
    this.zones = {
      mammals: [],
      birds: [],
      fishes: [],
      reptiles: [],
      others: []
    };
  }

  addAnimal(animalObj) {
    const animalZone = animalObj.zone;
    if ( this.zones[animalZone] !== undefined ) {
      this.zones[animalZone].push(animalObj);
    } else {
      this.zones.others.push(animalObj);
    }
  }

  removeAnimal(animalName) {
    for(let zone in this.zones) {
      let index;

      this.zones[zone].forEach((item, i) => {
        if( item.name === animalName ) {
          index = i;
        } else {
          index = -1;
        }
      });
      
      if(index > -1) {
        this.zones[zone].splice(index, 1);
      }
    }
  }

  getAnimal(type, value) {
    let result; 
    if(type === 'name') {
      result = Object.keys(this.zones).reduce((prev, key) => {
        let res = this.zones[key].find(item => item.name == value);
        return res || prev;
      }, 0);
    } else if(type === 'type') {
      result = Object.keys(this.zones).reduce((prev, key) => {
        let res = this.zones[key].filter(item => item.type === value);
        return [...prev, ...res];
      }, []);
    } else {
      console.error('Type error');
    }

    return result;
  }

  countAnimals() {
    this.animalCount = Object.keys(this.zones).reduce((sum, key) => {
      return sum + this.zones[key].length;
    }, 0);

    console.log(`Animals: ${this.animalCount}`);
  }
}

export default Zoo;