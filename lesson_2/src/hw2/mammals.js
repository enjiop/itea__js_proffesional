import Animal from './animal';

class Mammals extends Animal {
    constructor(name, phrase, foodType, speed) {
        super(name, phrase, foodType);
        this.type = 'wolf';
        this.zone = 'mammals';
        this.speed = speed;
    }

    run() {
        console.log(`${this.type} ${this.name} run with speed ${this.speed} km/h`);
    }
}

export default Mammals;