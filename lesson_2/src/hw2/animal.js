class Animal {
    constructor(name, phrase, foodType) {
        this.name = name;
        this.phrase = phrase;
        this.foodType = foodType;
    }

    eatSomething() {
        console.log('Mmmmm..');
    }
};

export default Animal;