import Animal from './animal';

class Reptiles extends Animal {
    constructor(name, phrase, foodType, speed) {
        super(name, phrase, foodType);
        this.type = 'lizard';
        this.zone = 'reptiles';
        this.speed = lizardspeed;
    }

    swim() {
        console.log(`${this.type} ${this.name} swim with speed ${this.speed} km/h`);
    }
}

export default Reptiles;