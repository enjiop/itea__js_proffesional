import Animal from './animal';

class Birds extends Animal {
    constructor(name, phrase, foodType, speed) {
        super(name, phrase, foodType);
        this.type = 'dove';
        this.zone = 'birds';
        this.speed = speed;
    }

    fly() {
        console.log(`${this.type} ${this.name} fly with speed ${this.speed} km/h`);
    }
}

export default Birds;