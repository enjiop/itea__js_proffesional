/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/hw1/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/hw1/burger.js":
/*!***************************!*\
  !*** ./src/hw1/burger.js ***!
  \***************************/
/*! exports provided: OurMenu, Burger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"OurMenu\", function() { return OurMenu; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Burger\", function() { return Burger; });\nlet OurMenu = [];\r\n\r\nfunction Burger(name, ingredients, cookingTime) {\r\n    this.name = name;\r\n    this.ingredients = ingredients;\r\n    this.cookingTime = cookingTime;\r\n\r\n    OurMenu.push(this);\r\n    console.log('menu:', OurMenu);\r\n\r\n    this.showComposition = function() {\r\n        let {ingredients, name} = this;\r\n        let ingredientsLength = ingredients.length;\r\n        console.log(ingredientsLength);\r\n\r\n        if( ingredientsLength !== 0 ) {\r\n            ingredients.map( item => console.log('Состав бургера', name, item));\r\n        }\r\n    }\r\n}\n\n//# sourceURL=webpack:///./src/hw1/burger.js?");

/***/ }),

/***/ "./src/hw1/helpers.js":
/*!****************************!*\
  !*** ./src/hw1/helpers.js ***!
  \****************************/
/*! exports provided: randomInt */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"randomInt\", function() { return randomInt; });\nfunction randomInt(min, max) {\r\n    return Math.floor(min + Math.random() * (max + 1 - min));\r\n}\n\n//# sourceURL=webpack:///./src/hw1/helpers.js?");

/***/ }),

/***/ "./src/hw1/index.js":
/*!**************************!*\
  !*** ./src/hw1/index.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _burger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./burger */ \"./src/hw1/burger.js\");\n/* harmony import */ var _order__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./order */ \"./src/hw1/order.js\");\n\r\n\r\n\r\nconst Ingredients = [\r\n    'Булка',\r\n    'Огурчик',\r\n    'Котлетка',\r\n    'Бекон',\r\n    'Рыбная котлета',\r\n    'Соус карри',\r\n    'Кисло-сладкий соус',\r\n    'Помидорка',\r\n    'Маслины',\r\n    'Острый перец',\r\n    'Капуста',\r\n    'Кунжут',\r\n    'Сыр Чеддер',\r\n    'Сыр Виолла',\r\n    'Сыр Гауда',\r\n    'Майонез'\r\n];\r\n\r\nlet ham = new _burger__WEBPACK_IMPORTED_MODULE_0__[\"Burger\"]('Hamburger', ['Булка', 'Огурчик', 'Котлетка'], 20);\r\nlet chis = new _burger__WEBPACK_IMPORTED_MODULE_0__[\"Burger\"]('Cheeseburger', ['Булка', 'Огурчик', 'Котлетка', 'Сыр Чеддер'], 25);\r\nlet bur = new _burger__WEBPACK_IMPORTED_MODULE_0__[\"Burger\"]('SomeBurg', ['Булка', 'Огурчик', 'Котлетка', 'Сыр Виолла', 'Майонез'], 30);\r\n\r\nnew _order__WEBPACK_IMPORTED_MODULE_1__[\"Order\"]('Hamburger');\r\nnew _order__WEBPACK_IMPORTED_MODULE_1__[\"Order\"]('', 'has', 'Майонез');\r\nnew _order__WEBPACK_IMPORTED_MODULE_1__[\"Order\"]('', 'has', 'Кунжут');\r\n\r\nnew _order__WEBPACK_IMPORTED_MODULE_1__[\"Order\"]('', 'except', 'Сыр Чеддер');\n\n//# sourceURL=webpack:///./src/hw1/index.js?");

/***/ }),

/***/ "./src/hw1/order.js":
/*!**************************!*\
  !*** ./src/hw1/order.js ***!
  \**************************/
/*! exports provided: OurOrders, Order */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"OurOrders\", function() { return OurOrders; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Order\", function() { return Order; });\n/* harmony import */ var _burger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./burger */ \"./src/hw1/burger.js\");\n/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers */ \"./src/hw1/helpers.js\");\n\r\n\r\n\r\nlet OurOrders = [];\r\n\r\nfunction Order(name, condition, value) {\r\n    this.id = new Date().getTime();\r\n    this.name = name;\r\n    this.condition = condition;\r\n    this.value = value;\r\n\r\n    this.newOrder = function (burger) {\r\n        OurOrders.push(this);\r\n        console.log(`Заказ ${OurOrders.length}: Бургер ${burger.name}, будет готов через ${burger.cookingTime}`);\r\n    }\r\n\r\n    this.burgerNotFound = function () {\r\n        let randomBurger = _burger__WEBPACK_IMPORTED_MODULE_0__[\"OurMenu\"][Object(_helpers__WEBPACK_IMPORTED_MODULE_1__[\"randomInt\"])(0, _burger__WEBPACK_IMPORTED_MODULE_0__[\"OurMenu\"].length - 1)];\r\n        let isAgree = false;\r\n\r\n        if (confirm(`К сожалению, такого бургера у нас нет, можете попробовать ${randomBurger.name}`)) {\r\n            this.newOrder(randomBurger);\r\n        } else {\r\n            while (!isAgree) {\r\n                if (confirm(`Можете попробовать ${randomBurger.name}`)) {\r\n                    isAgree = true;\r\n                    this.newOrder(randomBurger);\r\n                } else {\r\n                    isAgree = false;\r\n                    randomBurger = _burger__WEBPACK_IMPORTED_MODULE_0__[\"OurMenu\"][Object(_helpers__WEBPACK_IMPORTED_MODULE_1__[\"randomInt\"])(0, _burger__WEBPACK_IMPORTED_MODULE_0__[\"OurMenu\"].length - 1)];\r\n                }\r\n            }\r\n        }\r\n    }\r\n\r\n    this.order = function () {\r\n        if (this.name !== '') {\r\n            let result = _burger__WEBPACK_IMPORTED_MODULE_0__[\"OurMenu\"].find(item => item.name === this.name);\r\n            this.newOrder(result);\r\n        } else {\r\n            if (this.condition === 'has') {\r\n                let result = _burger__WEBPACK_IMPORTED_MODULE_0__[\"OurMenu\"].find(item => item.ingredients.indexOf(this.value) + 1);\r\n                if (result) {\r\n                    this.newOrder(result);\r\n                } else {\r\n                    this.burgerNotFound();\r\n                }\r\n            } else if (this.condition === 'except') {\r\n                let result = _burger__WEBPACK_IMPORTED_MODULE_0__[\"OurMenu\"].find(item => !(item.ingredients.indexOf(this.value) + 1));\r\n                if (result) {\r\n                    this.newOrder(result);\r\n                } else {\r\n                    this.burgerNotFound();\r\n                }\r\n            }\r\n        }\r\n    };\r\n\r\n    this.order();\r\n}\n\n//# sourceURL=webpack:///./src/hw1/order.js?");

/***/ })

/******/ });