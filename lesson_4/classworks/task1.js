import { BackendDeveloper, FrontendDeveloper, Designer, ProjectManager } from './composition';
/*

    Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
    расспределять и создавать сотрудников компании нужного типа.

    Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

    HeadHunt => {
        hire( obj ){
        ...
        }
    }

    Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики
*/

const url = 'http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2';

class HeadHunt {
    hire( obj ) {
        let EmployeeClass = null;
        if( obj.type === 'backend' ) {
            EmployeeClass = BackendDeveloper;
        } else if( obj.type === 'frontend' ) {
            EmployeeClass = FrontendDeveloper;
        } else if( obj.type === 'design' ) {
            EmployeeClass = Designer;
        } else if( obj.type === 'project' ) {
            EmployeeClass = ProjectManager;
        } else {
            return false;
        }
        return new EmployeeClass( obj );
    }
}

function getData(data) {
    const headHunt = new HeadHunt();
    const employee = data.map( item => {
        return headHunt.hire(item);   
    });
    console.log( employee );
}

const task1 = () => {
    let data = fetch(url)
        .then( res => res.json() )
        .then( getData );
};

export default task1;

