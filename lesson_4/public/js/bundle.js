/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./classworks/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./classworks/composition.js":
/*!***********************************!*\
  !*** ./classworks/composition.js ***!
  \***********************************/
/*! exports provided: BackendDeveloper, FrontendDeveloper, Designer, ProjectManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BackendDeveloper\", function() { return BackendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FrontendDeveloper\", function() { return FrontendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Designer\", function() { return Designer; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProjectManager\", function() { return ProjectManager; });\nvar MakeBackendMagic = function MakeBackendMagic(state) {\n  return {\n    makeBackendMagic: function makeBackendMagic() {\n      return console.log('Backend magic!');\n    }\n  };\n};\n\nvar MakeFrontendMagic = function MakeFrontendMagic(state) {\n  return {\n    makeFrontendMagic: function makeFrontendMagic() {\n      return console.log('Frontend magic!');\n    }\n  };\n};\n\nvar MakeItLooksBeautiful = function MakeItLooksBeautiful(state) {\n  return {\n    makeItLooksBeautiful: function makeItLooksBeautiful() {\n      return console.log('Looks beautiful!');\n    }\n  };\n};\n\nvar DistributeTasks = function DistributeTasks(state) {\n  return {\n    distributeTasks: function distributeTasks() {\n      return console.log('Distribute!');\n    }\n  };\n};\n\nvar DrinkSomeTea = function DrinkSomeTea(state) {\n  return {\n    drinkSomeTea: function drinkSomeTea() {\n      return console.log('Drink tea');\n    }\n  };\n};\n\nvar WatchYoutube = function WatchYoutube(state) {\n  return {\n    watchYoutube: function watchYoutube() {\n      return console.log('Whatch youtube');\n    }\n  };\n};\n\nvar Procrastinate = function Procrastinate(state) {\n  return {\n    procrastinate: function procrastinate() {\n      return console.log('Procrastinate');\n    }\n  };\n};\n\nfunction BackendDeveloper(_ref) {\n  var name = _ref.name,\n      gender = _ref.gender,\n      age = _ref.age,\n      _ref$type = _ref.type,\n      type = _ref$type === void 0 ? 'backend' : _ref$type;\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = type;\n  return Object.assign(this, MakeFrontendMagic(this), DrinkSomeTea(this), Procrastinate(this));\n}\nfunction FrontendDeveloper(_ref2) {\n  var name = _ref2.name,\n      gender = _ref2.gender,\n      age = _ref2.age,\n      _ref2$type = _ref2.type,\n      type = _ref2$type === void 0 ? 'frontend' : _ref2$type;\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = type;\n  return Object.assign(this, MakeBackendMagic(this), DrinkSomeTea(this), WatchYoutube(this));\n}\nfunction Designer(_ref3) {\n  var name = _ref3.name,\n      gender = _ref3.gender,\n      age = _ref3.age,\n      _ref3$type = _ref3.type,\n      type = _ref3$type === void 0 ? 'design' : _ref3$type;\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = type;\n  return Object.assign(this, MakeItLooksBeautiful(this), WatchYoutube(this), Procrastinate(this));\n}\nfunction ProjectManager(_ref4) {\n  var name = _ref4.name,\n      gender = _ref4.gender,\n      age = _ref4.age,\n      _ref4$type = _ref4.type,\n      type = _ref4$type === void 0 ? 'project' : _ref4$type;\n  this.name = name;\n  this.gender = gender;\n  this.age = age;\n  this.type = type;\n  return Object.assign(this, DistributeTasks(this), Procrastinate(this), DrinkSomeTea(this));\n}\n\n//# sourceURL=webpack:///./classworks/composition.js?");

/***/ }),

/***/ "./classworks/index.js":
/*!*****************************!*\
  !*** ./classworks/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _task1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./task1 */ \"./classworks/task1.js\");\n/* harmony import */ var _task2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./task2 */ \"./classworks/task2.js\");\n\n // task1();\n\nObject(_task2__WEBPACK_IMPORTED_MODULE_1__[\"default\"])();\n\n//# sourceURL=webpack:///./classworks/index.js?");

/***/ }),

/***/ "./classworks/task1.js":
/*!*****************************!*\
  !*** ./classworks/task1.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _composition__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./composition */ \"./classworks/composition.js\");\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\n\n/*\r\n\r\n    Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет\r\n    расспределять и создавать сотрудников компании нужного типа.\r\n\r\n    Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\r\n\r\n    HeadHunt => {\r\n        hire( obj ){\r\n        ...\r\n        }\r\n    }\r\n\r\n    Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики\r\n*/\n\nvar url = 'http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2';\n\nvar HeadHunt =\n/*#__PURE__*/\nfunction () {\n  function HeadHunt() {\n    _classCallCheck(this, HeadHunt);\n  }\n\n  _createClass(HeadHunt, [{\n    key: \"hire\",\n    value: function hire(obj) {\n      var EmployeeClass = null;\n\n      if (obj.type === 'backend') {\n        EmployeeClass = _composition__WEBPACK_IMPORTED_MODULE_0__[\"BackendDeveloper\"];\n      } else if (obj.type === 'frontend') {\n        EmployeeClass = _composition__WEBPACK_IMPORTED_MODULE_0__[\"FrontendDeveloper\"];\n      } else if (obj.type === 'design') {\n        EmployeeClass = _composition__WEBPACK_IMPORTED_MODULE_0__[\"Designer\"];\n      } else if (obj.type === 'project') {\n        EmployeeClass = _composition__WEBPACK_IMPORTED_MODULE_0__[\"ProjectManager\"];\n      } else {\n        return false;\n      }\n\n      return new EmployeeClass(obj);\n    }\n  }]);\n\n  return HeadHunt;\n}();\n\nfunction getData(data) {\n  var headHunt = new HeadHunt();\n  var employee = data.map(function (item) {\n    return headHunt.hire(item);\n  });\n  console.log(employee);\n}\n\nvar task1 = function task1() {\n  var data = fetch(url).then(function (res) {\n    return res.json();\n  }).then(getData);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (task1);\n\n//# sourceURL=webpack:///./classworks/task1.js?");

/***/ }),

/***/ "./classworks/task2.js":
/*!*****************************!*\
  !*** ./classworks/task2.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _task1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./task1 */ \"./classworks/task1.js\");\n\n/*\r\n    Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в\r\n                соответсвии с правилавми ниже:\r\n\r\n    1. Написать кастомные события которые будут менять статус светофора:\r\n    - start: включает зеленый свет\r\n    - stop: включает красный свет\r\n    - night: включает желтый свет, который моргает с интервалом в 1с.\r\n    И зарегистрировать каждое через addEventListener на каждом из светофоров.\r\n\r\n    2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,\r\n        чтобы включить режим \"нерегулируемого перекрестка\" (моргающий желтый).\r\n\r\n    3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)\r\n        или зеленый (на второй клик) цвет соотвественно.\r\n        Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.\r\n\r\n    4.  + Бонус: На кнопку \"Start Night\" повесить сброс всех светофоров с их текущего\r\n        статуса, на мигающий желтые.\r\n        Двойной, тройной и более клики на кнопку не должны вызывать повторную\r\n        инициализацию инвервала.\r\n*/\n\nvar startEvent = new CustomEvent('start', {\n  detail: {\n    color: 'green'\n  }\n});\nvar stopEvent = new CustomEvent('stop', {\n  detail: {\n    color: 'red'\n  }\n});\nvar nightEvent = new CustomEvent('night', {\n  detail: {\n    green: false,\n    red: false,\n    yellow: true\n  }\n});\n\nfunction handleNightEvent(e) {\n  e.target.className = 'trafficLight yellow';\n}\n\nvar task2 = function task2() {\n  document.addEventListener('DOMContentLoaded', function () {\n    var startNightBtn = document.getElementById('Do');\n    var trafficLights = document.querySelectorAll('.trafficLight');\n    trafficLights.forEach(function (item) {\n      item.addEventListener('nightEvent', handleNightEvent);\n    });\n  });\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (task2);\n\n//# sourceURL=webpack:///./classworks/task2.js?");

/***/ })

/******/ });